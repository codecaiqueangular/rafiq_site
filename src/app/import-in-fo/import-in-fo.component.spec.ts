import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportInFoComponent } from './import-in-fo.component';

describe('ImportInFoComponent', () => {
  let component: ImportInFoComponent;
  let fixture: ComponentFixture<ImportInFoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportInFoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportInFoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
