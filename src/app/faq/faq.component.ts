import { Component, OnInit } from '@angular/core';
import { APIservicesService } from '../apiservices.service';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {

  
  language=localStorage.getItem('locale');
  allquestiontype:any[];
  showSpinner:boolean=true
  imageURL:string=localStorage.getItem('imageURL');

  constructor(private _service: APIservicesService, private router: Router ) { }

  ngOnInit() {
    console.log(this.language)

             //show_allquestiontype//
       this._service.show_allquestiontype(this.language)
             .subscribe(
               data => {
                 let resources: any[] = data["data"];
                  this.allquestiontype = resources;
                  this.showSpinner=false;
          });





   }
   
  }