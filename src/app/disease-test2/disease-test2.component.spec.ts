import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiseaseTest2Component } from './disease-test2.component';

describe('DiseaseTest2Component', () => {
  let component: DiseaseTest2Component;
  let fixture: ComponentFixture<DiseaseTest2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiseaseTest2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiseaseTest2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
