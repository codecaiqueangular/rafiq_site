import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot
} from "@angular/router"
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    if (localStorage.getItem('user_id')!="null"&&localStorage.getItem('user_id')!="") {
      

      return true;
      } else {
        this.router.navigate(['/Login'], {
          queryParams: {
            return: state.url
          }
        });
        return false;
      }

    }

}
