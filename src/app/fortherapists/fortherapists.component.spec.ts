import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FortherapistsComponent } from './fortherapists.component';

describe('FortherapistsComponent', () => {
  let component: FortherapistsComponent;
  let fixture: ComponentFixture<FortherapistsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FortherapistsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FortherapistsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
