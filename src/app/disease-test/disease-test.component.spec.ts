import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiseaseTESTComponent } from './disease-test.component';

describe('DiseaseTESTComponent', () => {
  let component: DiseaseTESTComponent;
  let fixture: ComponentFixture<DiseaseTESTComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiseaseTESTComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiseaseTESTComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
