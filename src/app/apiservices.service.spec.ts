import { TestBed } from '@angular/core/testing';

import { APIservicesService } from './apiservices.service';

describe('APIservicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: APIservicesService = TestBed.get(APIservicesService);
    expect(service).toBeTruthy();
  });
});
