import { BrowserModule } from '@angular/platform-browser';
import { NgModule ,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { FormsModule ,ReactiveFormsModule} from '@angular/forms';

// import ngx-translate and the http loader
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { Blog1Component } from './blog1/blog1.component';
import { BlogPostComponent } from './blog-post/blog-post.component';
import { BookingPageComponent } from './booking-page/booking-page.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { DetailPage2Component } from './detail-page2/detail-page2.component';

import { FaqComponent } from './faq/faq.component';
import { GridListComponent } from './grid-list/grid-list.component';
import { IndexComponent } from './index/index.component';
import { LoginComponent } from './login/login.component';
import { Login2Component } from './code/login2.component';
import { RegisterComponent } from './register/register.component';
import { RegisterDoctorComponent } from './register-doctor/register-doctor.component';
import { SubmitReviewComponent } from './submit-review/submit-review.component';
import { AppRoutingModule } from './app-routing.module';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { NgwWowModule } from 'ngx-wow';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {Ng2PageScrollModule} from 'ng2-page-scroll';

import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { PsychologicalComponent } from './psychological/psychological.component';
import { FortherapistsComponent } from './fortherapists/fortherapists.component';
import { AllNEWSComponent } from './all-news/all-news.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { TermsComponent } from './terms/terms.component';
import { DiseaseTESTComponent } from './disease-test/disease-test.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {  MatStepperModule} from '@angular/material';
import { ResultTestComponent } from './result-test/result-test.component';
import { ProfileComponent } from './profile/profile.component';
import { RatingModule } from 'ngx-bootstrap/rating';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { DetailsFAQComponent } from './details-faq/details-faq.component';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ImportInFoComponent } from './import-in-fo/import-in-fo.component';
import { CreateClientComponent } from './create-client/create-client.component';
import { SafePipe } from './safe.pipe';
import { DiseaseTest2Component } from './disease-test2/disease-test2.component';
import {MatProgressBarModule} from '@angular/material';
import { TabsModule } from 'ngx-bootstrap/tabs'
import { GoogleChartsModule } from 'angular-google-charts';
import { ChartsModule } from 'ng2-charts';
 import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { PostsCatogeriesComponent } from './posts-catogeries/posts-catogeries.component';
// Angular Notifier
import { NotifierModule,NotifierOptions } from "angular-notifier";
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { Fridlist2Component } from './fridlist2/fridlist2.component';
import { UpdateProfileComponent } from './update-profile/update-profile.component';

import { CookieService } from 'ngx-cookie-service';


import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../environments/environment';
import { MessagingService } from './messaging.service';
import { AngularFireMessagingModule } from '@angular/fire/messaging';



 const customNotifierOptions: NotifierOptions = {
  position: {
    horizontal: {
      position: 'right',
      distance: 12
    },
    vertical: {
      position: 'top',
      distance: 12,
      gap: 10
    }
  },
  theme: 'uifort',
  behaviour: {
    autoHide: 5000,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 300,
     easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 300,
      easing: 'ease',
      offset: 50
    },
    shift: {
      speed: 300,
     easing: 'ease'
    },
    overlap: 150
  }
};  
// Example components
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    Blog1Component,
    BlogPostComponent,
    BookingPageComponent,
    ConfirmComponent,
    DetailPage2Component,

    FaqComponent,
    GridListComponent,
    IndexComponent,
    LoginComponent,
    Login2Component,
    RegisterComponent,
    RegisterDoctorComponent,
    SubmitReviewComponent,

    PsychologicalComponent,
    FortherapistsComponent,
    AllNEWSComponent,
    PrivacyComponent,
    TermsComponent,
    DiseaseTESTComponent,
    ResultTestComponent,
    ProfileComponent,
    DetailsFAQComponent,
    ImportInFoComponent,
    CreateClientComponent,
    SafePipe,
    DiseaseTest2Component,
    ForgetPasswordComponent,
    ResetPasswordComponent,
    PostsCatogeriesComponent,
    Fridlist2Component,
    UpdateProfileComponent,
    


    
  ],
  imports: [

    // fire base 
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule, // for database

AngularFireMessagingModule,
    BrowserModule,
    AppRoutingModule,
    NgwWowModule,
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),  
    ChartsModule,

    MatProgressBarModule, 
    ReactiveFormsModule,
        Ng2PageScrollModule,CarouselModule.forRoot(), AngularFontAwesomeModule,BrowserAnimationsModule,MatStepperModule,  
        // ngx-translate and the loader module
        HttpClientModule,
        GoogleChartsModule,
        NgbModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient],
          },
        }),
  
        RatingModule.forRoot(), FormsModule, PaginationModule.forRoot(), AccordionModule.forRoot(), ModalModule.forRoot(), TabsModule.forRoot(),
        NotifierModule,NotifierModule.withConfig(customNotifierOptions), TimepickerModule.forRoot(), 






  ],
  providers: [ CookieService,MessagingService],
  bootstrap: [AppComponent,],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],


  
})
export class AppModule { }


