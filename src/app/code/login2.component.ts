import { Component, OnInit } from '@angular/core';
import { APIservicesService } from '../apiservices.service';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-login2',
  templateUrl: './login2.component.html',
  styleUrls: ['./login2.component.css']
})
export class Login2Component implements OnInit {
  showSpinner:boolean=false;
  userid: string=localStorage.getItem('user_id');
  idtest: string=localStorage.getItem('idtest');
  ID: string=localStorage.getItem('id');
  report_id;
  
  constructor(private _service: APIservicesService, private router: Router) { }
  ngOnInit() {
  }


  submitForm(code){
    this.showSpinner=true;

  
console.log(this.userid,code.value,this.idtest)
    this._service.el3amil_code(this.userid,code.value,this.idtest).subscribe(
      data=>  {

        let error:number= data["error"];

        if(error==0){
          if(localStorage.getItem('locale')=='en')
          {

            Swal.fire({
              icon: 'success',
              title: 'code entered successfully',
              showConfirmButton: false,
              timer: 2000
            })

          }

          else
          {

            Swal.fire({
              icon: 'success',
              title: 'تم ادخال الكود بنجاح  ',
              showConfirmButton: false,
              timer: 2000
            })

          }

    
   
          this.router.navigate(['/DiseaseTEST',this.ID])
       
        }

        else
        {

          if(localStorage.getItem('locale')=='en')
          {

            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'code incorrect, write correct code',
            })
           

          }

          else
          {

            Swal.fire({
              icon: 'error',
              title: 'اوبس...',
              text: 'الكود خظأ ,اكتب الكود الصحيح',
            })

          }

          this.showSpinner=false;


        }

        let report_id:number= data["report_id"];
        this.report_id=report_id;
        localStorage.setItem( 'report_id',this.report_id);
  

      }

    );
  }

}
