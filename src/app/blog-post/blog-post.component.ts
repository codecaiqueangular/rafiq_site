import { Component, OnInit } from '@angular/core';
import $ from 'jquery';
import { Router,ActivatedRoute,ParamMap } from '@angular/router';
import { APIservicesService } from '../apiservices.service';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-blog-post',
  templateUrl: './blog-post.component.html',
  styleUrls: ['./blog-post.component.css']
})
export class BlogPostComponent implements OnInit {
  showSpinner:boolean=true;
  blogid:number;
  language=localStorage.getItem('locale');
  posts:any;
  recent_posts:any[];
  category:any[];
  imageURL:string=localStorage.getItem('imageURL');

  constructor(private _service: APIservicesService, private router: Router,private route:ActivatedRoute ) { }

  ngOnInit() {
    this.blogid=+this.route.snapshot.paramMap.get('id');


    //show_question_answers//
this._service.show_onepost(this.blogid,this.language)
    .subscribe(
      data => {
        let resources: any = data["posts"];
        let resources1: any[] = data["recent_posts"];
        let resources2: any[] = data["category"];
      


         this.posts = resources;
         this.recent_posts = resources1;
         this.category = resources2;
       this.showSpinner=false




 });









 
  }

}
