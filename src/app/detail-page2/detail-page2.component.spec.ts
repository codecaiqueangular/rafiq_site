import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailPage2Component } from './detail-page2.component';

describe('DetailPage2Component', () => {
  let component: DetailPage2Component;
  let fixture: ComponentFixture<DetailPage2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailPage2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailPage2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
