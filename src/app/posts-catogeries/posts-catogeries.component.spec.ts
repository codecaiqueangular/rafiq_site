import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostsCatogeriesComponent } from './posts-catogeries.component';

describe('PostsCatogeriesComponent', () => {
  let component: PostsCatogeriesComponent;
  let fixture: ComponentFixture<PostsCatogeriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostsCatogeriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostsCatogeriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
