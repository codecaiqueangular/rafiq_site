import { Component, OnInit } from '@angular/core';
import { APIservicesService } from '../apiservices.service';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-posts-catogeries',
  templateUrl: './posts-catogeries.component.html',
  styleUrls: ['./posts-catogeries.component.css']
})
export class PostsCatogeriesComponent implements OnInit {
  language=localStorage.getItem('locale');
  blogid:number;
  recent_posts:any[];
  category:any[];
  posts:any[];
  imageURL:string=localStorage.getItem('imageURL');
  showSpinner:boolean=true;
  constructor(private _service: APIservicesService, private router: Router,private route:ActivatedRoute ) { }

  ngOnInit() {
    this.blogid=+this.route.snapshot.paramMap.get('id');

  
     //show_posts_incategory//
     this._service.show_posts_incategory(this.blogid,this.language)
     .subscribe(
       data => {
         let resources: any = data["data"];
          this.posts = resources;
          this.showSpinner=false
     
     
     });
     

            



   }
   
  }

