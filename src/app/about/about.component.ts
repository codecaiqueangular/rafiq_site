import { Component, OnInit } from '@angular/core';
import { APIservicesService } from '../apiservices.service';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  language=localStorage.getItem('locale');
  max: number = 5;
  isReadonly: boolean = true;
  about:any;
  allopinions:any[];
  showSpinner:boolean=true;
  imageURL:string=localStorage.getItem('imageURL');
   ImageURl="./assets/img/default.png"

  constructor(private _service: APIservicesService, private router: Router ) { }

  ngOnInit() {

  

                //show_aboutus//
                this._service.show_aboutus(this.language)
                .subscribe(
                  data => {
                    let resources: any= data["data"];
                     this.about = resources;
                     this.showSpinner=false
             });


                   //show_allopinions//
                   this._service.show_allopinions()
                   .subscribe(
                     data => {
                       let resources: any[]= data["data"];
                        this.allopinions = resources;
                        this.showSpinner=false
                });


   

                       





   }
   
  }

