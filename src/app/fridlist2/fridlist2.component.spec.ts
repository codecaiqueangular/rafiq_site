import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Fridlist2Component } from './fridlist2.component';

describe('Fridlist2Component', () => {
  let component: Fridlist2Component;
  let fixture: ComponentFixture<Fridlist2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Fridlist2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Fridlist2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
