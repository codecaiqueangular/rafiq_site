import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllNEWSComponent } from './all-news.component';

describe('AllNEWSComponent', () => {
  let component: AllNEWSComponent;
  let fixture: ComponentFixture<AllNEWSComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllNEWSComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllNEWSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
