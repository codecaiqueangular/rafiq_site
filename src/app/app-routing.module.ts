

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuthenticationService  } from "./authentication.service";
import { AboutComponent } from './about/about.component';
import { Blog1Component } from './blog1/blog1.component';
import { BlogPostComponent } from './blog-post/blog-post.component';
import { BookingPageComponent } from './booking-page/booking-page.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { DetailPage2Component } from './detail-page2/detail-page2.component';

import { FaqComponent } from './faq/faq.component';
import { GridListComponent } from './grid-list/grid-list.component';
import { IndexComponent } from './index/index.component';
import { LoginComponent } from './login/login.component';
import { Login2Component } from './code/login2.component';
import { RegisterComponent } from './register/register.component';
import { RegisterDoctorComponent } from './register-doctor/register-doctor.component';
import { SubmitReviewComponent } from './submit-review/submit-review.component';

import { PsychologicalComponent } from './psychological/psychological.component';
import { FortherapistsComponent } from './fortherapists/fortherapists.component';
import { AllNEWSComponent } from './all-news/all-news.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { TermsComponent } from './terms/terms.component';
import { DiseaseTESTComponent } from './disease-test/disease-test.component';
import { ResultTestComponent } from './result-test/result-test.component';
import { ProfileComponent } from './profile/profile.component';
import { DetailsFAQComponent } from './details-faq/details-faq.component';
import { ImportInFoComponent } from './import-in-fo/import-in-fo.component';
import { CreateClientComponent } from './create-client/create-client.component';
import { DiseaseTest2Component } from './disease-test2/disease-test2.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { PostsCatogeriesComponent } from './posts-catogeries/posts-catogeries.component';
import { Fridlist2Component } from './fridlist2/fridlist2.component';
import { UpdateProfileComponent } from './update-profile/update-profile.component';

const routes: Routes = [
  { path: '', redirectTo: 'index', pathMatch:'full' },
  { path: 'index',  component:IndexComponent },
  { path: 'about', component: AboutComponent },
  { path: 'Blog1', component: Blog1Component },
  { path: 'BlogPost/:id', component: BlogPostComponent },
  { path: 'postscategory/:id', component: PostsCatogeriesComponent },
  { path: 'updateprofile', component: UpdateProfileComponent ,canActivate: [AuthenticationService] },

  { path: 'BookingPage/:id', component: BookingPageComponent,canActivate: [AuthenticationService] },
  { path: 'Confirm', component: ConfirmComponent },
  { path: 'DetailPage2/:id', component: DetailPage2Component },
  { path: 'Faq', component: FaqComponent },
  { path: 'Faq/:id', component: DetailsFAQComponent },
  { path: 'GridList', component: GridListComponent },
  { path: 'GridList2/:id', component: Fridlist2Component },

  { path: 'Login', component: LoginComponent },
  { path: 'code', component: Login2Component ,canActivate: [AuthenticationService]},
  { path: 'CreateClient', component: CreateClientComponent ,canActivate: [AuthenticationService]},

  { path: 'Register', component: RegisterComponent },
  { path: 'RegisterDoctor', component: RegisterDoctorComponent },
  { path: 'SubmitReview/:id', component: SubmitReviewComponent ,canActivate: [AuthenticationService] },

  { path: 'Psychological', component: PsychologicalComponent },
  { path: 'Fortherapists', component: FortherapistsComponent },
  { path: 'AllNEWS', component: AllNEWSComponent },
  { path: 'Privacy', component: PrivacyComponent },
  { path: 'Terms', component: TermsComponent },
  { path: 'DiseaseTEST/:id', component: DiseaseTESTComponent,canActivate: [AuthenticationService] },
  { path: 'DiseaseTEST2', component: DiseaseTest2Component,canActivate: [AuthenticationService] },
  { path: 'ResultTest/:id', component: ResultTestComponent,canActivate: [AuthenticationService] },
  { path: 'importinfo', component: ImportInFoComponent },
  { path: 'forgetpassword', component: ForgetPasswordComponent },
  { path: 'resetpassword', component: ResetPasswordComponent },

  { path: 'profile', component: ProfileComponent ,canActivate: [AuthenticationService]},

  { path:  '**', redirectTo: 'index', pathMatch:  'full'},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }